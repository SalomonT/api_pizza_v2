const Pool = require('pg').Pool
const pool = new Pool({

  // host: 'localhost',
  // port: '5432',
  // database: 'pizza2_v2',
  // user: 'postgres',
  // password: 'saloSql',

  host:       'dpg-cj0sars07spjv9rg9p10-a.oregon-postgres.render.com',
  port:       '5432',
  database:   'api_pizza_v2',
  user:       'salo',
  password:   'K3f1ivK6aJGBP1Z1a6lAAgIPEOUr8CXH',
  ssl:        true,
})

// PGPASSWORD=K3f1ivK6aJGBP1Z1a6lAAgIPEOUr8CXH psql -h dpg-cj0sars07spjv9rg9p10-a.oregon-postgres.render.com -U salo api_pizza_v2
// ___________________________________________
//   ___________________________________________
//   ___________consultas tabla pizza
//   ___________________________________________
//   ___________________________________________
const getPizzas = (request, response) => {
  pool.query('SELECT * FROM pizza', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}


const getPizzasById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM pizza WHERE piz_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}
const getPizzasByName = (request, response) => {
  const name = request.params.name;
  const queryAux = "SELECT * FROM pizza WHERE lower(piz_name) Like lower(";
  const queryAux2 = "'%" + name + "%'";
  const queryAux3 = ")";
  const realQuery = queryAux + queryAux2 + queryAux3;

  pool.query(realQuery, (error, results) => {
    if (error) {
      console.log(queryAux + queryAux2 + queryAux3)
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createPizza = (request, response) => {
  // const { piz_name, piz_description } = request.body
  const piz_name = request.params.piz_name;
  const piz_description = request.params.piz_description;
  console.log("mirando los datos: " + piz_name)
  pool.query('INSERT INTO pizza (piz_name, piz_description) VALUES ($1, $2)', [piz_name, piz_description], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`Pizza added with ID: ${results.piz_name}`)
  })
}

const updatePizza = (request, response) => {
  const id = parseInt(request.params.id)
  const piz_name = request.params.piz_name;
  const piz_description = request.params.piz_description;
  // const {piz_id, piz_name, piz_description} = request.body;

  pool.query(
    'UPDATE pizza SET piz_name = $1, piz_description = $2 WHERE piz_id = $3',
    [piz_name, piz_description, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`User modified with ID: ${id}`)
    }
  )
}

const deletePizza = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM pizza WHERE piz_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`User deleted with ID: ${id}`)
  })
}

//   ___________________________________________
//   ___________________________________________
//   ___________consultas tabla ingredients
//   ___________________________________________
//   ___________________________________________
const getIngredients = (request, response) => {
  pool.query('SELECT * FROM ingredients', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}


const getIngredientsById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM ingredients WHERE ing_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createIngredients = (request, response) => {
  const { descripcion, calorias } = request.body

  pool.query('INSERT INTO ingredients (ing_description, ing_calories) VALUES ($1, $2)', [descripcion, calorias], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`ingredients added with ID: ${results.insertId}`)
  })
}

const updateIngredients = (request, response) => {
  const id = parseInt(request.params.id)
  const { descripcion, calorias } = request.body

  pool.query(
    'UPDATE ingredients SET ing_description = $1, piz_calories = $2 WHERE piz_id = $3',
    [descripcion, calorias, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`ingredients modified with ID: ${id}`)
    }
  )
}

const deleteIngredients = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM ingredients WHERE ing_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`ingredients deleted with ID: ${id}`)
  })
}

//   ___________________________________________
//   ___________________________________________
//   ___________consultas tabla pizza_ingredients
//   ___________________________________________
//   ___________________________________________

const getPizzaIngredientsById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM pizza_ingredients WHERE piz_ing_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}


//   ___________________________________________
//   ___________________________________________
//   ___________consultas tabla usuarios
//   ____________________________________________
//   ___________________________________________

const getUsuarios = (request, response) => {
  pool.query('SELECT * FROM usuario', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}


const getUsuariosByNamePassword = (request, response) => {

  // const { nombre, contrasenia } = request.body;
  const user = request.params.user
  const pwd = request.params.pwd


  pool.query('SELECT * FROM usuario WHERE nombre = $1 and contrasenia = $2', [user, pwd],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
}
const getUsuariosByName = (request, response) => {
  const name = request.params.name;
  const queryAux = "SELECT * FROM usuario WHERE lower(nombre) Like lower(";
  const queryAux2 = "'%" + name + "%'";
  const queryAux3 = ")";
  const realQuery = queryAux + queryAux2 + queryAux3;

  pool.query(realQuery, (error, results) => {
    if (error) {
      console.log(queryAux + queryAux2 + queryAux3)
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createUsuario = (request, response) => {
  // const { piz_name, piz_description } = request.body
  const { id,nombre, contrasenia, correo } = request.body;
  // const  nombre= request.body;
  // const correo = request.body;

  // Get the total count of records in the 'usuario' table
  pool.query('SELECT COUNT(*) FROM usuario', (error, result) => {
    if (error) {
      throw error;
    }

    // Get the count from the result
    const totalRecords = result.rows[0].count;

    // Calculate the value for 'id' as the total count + 1
    const idAux = totalRecords + 1;


    console.log("mirando los datos: " + nombre)
    pool.query('INSERT INTO usuario (id,nombre, contrasenia, correo) VALUES ($1, $2,$3,$4)',
      [idAux, nombre, contrasenia, correo],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(201).send(`Pizza added with ID: ${idAux}`)
      })
  })
}


//   ___________________________________________
//   ___________________________________________
//   ___________MODULOS A EXPORTAR
//   ____________________________________________
//   ___________________________________________
module.exports = {
  // getPizzas,    getPizzasById, getPizzasByName, createPizza, updatePizza, deletePizza
  getPizzas, createPizza, deletePizza,
  getPizzasById,
  updatePizza,
  getPizzasByName,

  getIngredients,
  getIngredientsById, updateIngredients,
  deleteIngredients,
  getPizzaIngredientsById,

  createUsuario,
  getUsuarios,
  getUsuariosByNamePassword
}