// const {db} =require ('../config/cnn')

// async function consulta(id){
//     let result = await db.any(`select * from pizza
//                                 where piz_id =$1`,[id]);
//     console.table(result);

// }

// consulta(1);


//packages

const express =require('express');
const app =express();

const bodyParser = require('body-parser')
const db= require('../config/query');

//middlewears
app.use(express.json());
// app.use(express.urlencoded({extended:true}));

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

//routes
// app.use(require('./routes/index'));

//
app.get ('/', (req, res)=> {res.send('Simple pizza api running')})

app.get('/pizzas', db.getPizzas);
app.get('/pizzasId/:id', db.getPizzasById);
app.put('/pizzasId/:id/:piz_name/:piz_description', db.updatePizza);
app.get('/pizzasName/:name', db.getPizzasByName);
app.post('/newpizza/:piz_name/:piz_description', db.createPizza);
app.delete('/pizzasId/:id', db.deletePizza);


app.get('/ingredients', db.getIngredients);
app.put('/ingredientsId/:id', db.getIngredientsById);
app.put('/ingredientsId/:id', db.updateIngredients);


app.post('/pizzas/user',db.createUsuario)
app.get('/pizzas/user',db.getUsuarios)
app.post('/pizzas/user/auth/:user/:pwd',db.getUsuariosByNamePassword)





//Server Execute
const port = process.env.PORT || 3000;
app.listen(port);
console.log('Server run on : http://localhost:3000');



