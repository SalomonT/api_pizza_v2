'use strict'
const res = require('express/lib/response');
const{db} = require('../config/cnn');
const router = require('../src/routes');

const {pool}= require('../config/query');

// const getPizzas = async (req, res) =>{
//     const response =await db.any(`select * from pizza`);
//     res.json(response);
// }

// const getPizzasById = async (req, res) =>{
//     let id = req.params.id;
//     const response =await db.any(`select * from pizza where piz_id =$1::int`,[id]);
//     res.json(response);
// }

// const getPizzasByName = async (req, res) =>{
//     let name = req.params.name;
//     const response =await db.any(`select * from pizza where piz_name =$1`,[name]);
//     res.json(response);
// }
// //insercion a traves de query
// const createPizza =  async (req, res) =>{
//     const {name, description} = req.query;
//     const response =await db.any(`insert into pizza(piz_name, piz_description)
//                                  values ($1,$2)`,[name, description]);
//     res.json(response);


// }

// //actualizar pizza
// const updatePizza =  async (req, res) =>{
//     const {id,name, description} = req.query;
//     const response =await db.one(`update pizza set piz_name = $1, piz_description=$2
//                                  where piz_id = $3 returning*`,[name, description,id]);
//     res.json(response);


// }

// //Borrar pizza_description
// const deletePizza = async (req, res) => {
//     await deletePizzaIngredients(req, res);
//     let id= req.params.id;

//     let response = await db.query(`DELETE from pizza WHERE piz_id = $1`, [id]);
//     res.json(response);

// }
// let deletePizzaIngredients = async (req, res) => {
//     let id = req.params.id;
//     let response = await db.query(`DELETE from pizza_ingredients WHERE piz_id = $1`, [id]);
//     res.json(response)
// } 

// const getPizzas = (request, response) => {
//     pool.query('SELECT * FROM pizza BY id ASC', (error, results) => {
//       if (error) {
//         throw error
//       }
//       response.status(200).json(results.rows)
//     })
//   }

module.exports = {
    // getPizzas,    getPizzasById, getPizzasByName, createPizza, updatePizza, deletePizza
    getPizzas
}